﻿using UnityEngine;
using UnityEngine.SceneManagement;
using LiteNetLib;
using LiteNetLib.Utils;

public class NetClient : NetBase, INetEventListener {

    NetPeer connectedServer;
    NetDataWriter dataWriter;

    ChatClient chatClient;

    // Use this for initialization
    void Start() {
        if (FindObjectsOfType<NetClient>().Length > 1) {
            Destroy(gameObject);
        } else {
            DontDestroyOnLoad(gameObject);
        }

        netManager = new NetManager(this, NetBase.DefaultConnectKey);
        dataWriter = new NetDataWriter();
    }

    // Update is called once per frame
    void Update() {
        netManager.PollEvents();
    }

    public void StartClient() {
        if (netManager.Start()) {
            netManager.UpdateTime = NetBase.DefaultUpdateTime;
            ClientLog("Started.");

            ConnectTo(NetBase.LocalHost);
        } else {
            ClientLog("Failed to start.");
        }
    }

    public void ConnectTo(string ip, int port = NetBase.DefaultPort) {
        netManager.Connect(ip, port);
        ClientLog("Attempting to connect to " + ip + ":" + port + ".");
    }

    public void SendChatPacket(string chatMessage) {
        if (connectedServer != null) {
            dataWriter.Reset();
            dataWriter.Put(chatMessage);
            connectedServer.Send(dataWriter, SendOptions.ReliableOrdered);
        } else {
            ClientLog("Not connected to a server!");
        }
    }

    public void OnPeerConnected(NetPeer peer) {
        ClientLog("Connected to " + peer.EndPoint + ".");
        connectedServer = peer;

        SceneManager.LoadScene(1);
    }

    public void OnPeerDisconnected(NetPeer peer, DisconnectInfo disconnectInfo) {
        ClientLog("Disconnected from " + peer.EndPoint + ". Reason: " + disconnectInfo.Reason); 
        if (connectedServer.Equals(peer)) {
            connectedServer = null;
        }
    }

    public void OnNetworkError(NetEndPoint endPoint, int socketErrorCode) {
        ClientLog("Received error " + socketErrorCode + ".");
    }

    public void OnNetworkReceive(NetPeer peer, NetDataReader reader) {
        ClientLog("Received message from " + peer.EndPoint + ".");

        string chatMessage = reader.GetString();
        FindObjectOfType<ChatClient>().ReceiveChatMessage(chatMessage);
    }

    public void OnNetworkReceiveUnconnected(NetEndPoint remoteEndPoint, NetDataReader reader, UnconnectedMessageType messageType) {
        ClientLog("Received unconnected message: " + messageType + ".");
    }

    public void OnNetworkLatencyUpdate(NetPeer peer, int latency) {
        //throw new System.NotImplementedException();
    }

    private void ClientLog(string logMessage) {
        Debug.Log("[CLIENT] " + logMessage);
    }
}
