﻿using UnityEngine;
using LiteNetLib;

public class NetBase : MonoBehaviour {

    public const int MaxConnections = 100;
    public const string DefaultConnectKey = "ChatKey";
    public const string LocalHost = "127.0.0.1";
    public const int DefaultPort = 48035;
    public const int DefaultUpdateTime = 15;

    protected NetManager netManager;

    private void OnDestroy() {
        if (netManager != null) {
            netManager.Stop();
            netManager = null;
            Debug.Log("NetworkManager stopped!");
        }
    }
}
