﻿using System.Collections.Generic;
using UnityEngine;
using LiteNetLib;
using LiteNetLib.Utils;

public class NetServer : NetBase, INetEventListener {

    List<NetPeer> connectedClients;
    NetDataWriter dataWriter;

    // Use this for initialization
    void Start() {
        if (FindObjectsOfType<NetServer>().Length > 1) {
            Destroy(gameObject);
        } else {
            DontDestroyOnLoad(gameObject);
        }

        netManager = new NetManager(this, NetBase.MaxConnections, NetBase.DefaultConnectKey);
        dataWriter = new NetDataWriter();
        connectedClients = new List<NetPeer>();
    }

    // Update is called once per frame
    void Update() {
        netManager.PollEvents();
    }

    public void StartServer() {
        StartServer(NetBase.DefaultPort);
    }

    public void StartServer(int port) {
        if (netManager.Start(port)) {
            netManager.UpdateTime = NetBase.DefaultUpdateTime;
            ServerLog("Started listening on port " + port + ".");
        } else {
            ServerLog("Failed to start.");
        }
    }

    public void OnPeerConnected(NetPeer peer) {
        ServerLog("New client connected: " + peer.EndPoint);
        connectedClients.Add(peer);
    }

    public void OnPeerDisconnected(NetPeer peer, DisconnectInfo disconnectInfo) {
        ServerLog("Client disconnected: " + peer.EndPoint + ". Reason: " + disconnectInfo.Reason);
        connectedClients.Remove(peer);
    }

    public void OnNetworkError(NetEndPoint endPoint, int socketErrorCode) {
        ServerLog("Received error " + socketErrorCode + ".");
    }

    public void OnNetworkReceive(NetPeer peer, NetDataReader reader) {
        ServerLog("Received message from " + peer.EndPoint + ".");

        string chatMessage = reader.GetString();
        dataWriter.Reset();
        dataWriter.Put(chatMessage);
        netManager.SendToAll(dataWriter, SendOptions.ReliableOrdered);
    }

    public void OnNetworkReceiveUnconnected(NetEndPoint remoteEndPoint, NetDataReader reader, UnconnectedMessageType messageType) {
        ServerLog("Received unconnected message: " + messageType + ".");
    }

    public void OnNetworkLatencyUpdate(NetPeer peer, int latency) {
        //throw new System.NotImplementedException();
    }

    private void ServerLog(string logMessage) {
        Debug.Log("[SERVER] " + logMessage);
    }
}
