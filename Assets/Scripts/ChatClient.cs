﻿using UnityEngine;
using UnityEngine.UI;

public class ChatClient : MonoBehaviour {

    [SerializeField] Text chatHistory;

    NetClient netClient;

	// Use this for initialization
	void Start () {
        netClient = FindObjectOfType<NetClient>();
	}

    public void SendChatMessage(InputField inputField) {
        netClient.SendChatPacket(inputField.text);
        inputField.text = "";
    }

    public void ReceiveChatMessage(string chatMessage) {
        chatHistory.text += "\n" + chatMessage;
    }
}
